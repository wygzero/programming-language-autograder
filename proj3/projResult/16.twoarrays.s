	.data
.newline: .asciiz "\n"
	.text
	.globl main
main:	nop
	move	$fp,$sp
	add $s0, $gp, 204
	li $s1, 0
	sw $s1, 0($s0)
.L0:	nop
	add $s0, $gp, 204
	lw $s1, 0($s0)
	li $s0, 50
	slt $s2, $s1, $s0
	beq $s2, $zero, .L1
	add $s0, $gp, 204
	lw $s1, 0($s0)
	add $s0, $gp, 4
	sll $s1, $s1, 2
	add $s0, $s0, $s1
	li $s1, 0
	sw $s1, 0($s0)
	add $s0, $gp, 204
	add $s1, $gp, 204
	lw $s2, 0($s1)
	li $s1, 1
	add $s3, $s2, $s1
	sw $s3, 0($s0)
	j .L0
.L1:	 nop
	add $s0, $gp, 208
	li $s1, 0
	sw $s1, 0($s0)
.L2:	nop
	add $s0, $gp, 208
	lw $s1, 0($s0)
	li $s0, 50
	slt $s2, $s1, $s0
	beq $s2, $zero, .L3
	add $s0, $gp, 208
	lw $s1, 0($s0)
	add $s0, $gp, 216
	sll $s1, $s1, 2
	add $s0, $s0, $s1
	add $s1, $gp, 208
	lw $s2, 0($s1)
	li $s1, 50
	add $s3, $s2, $s1
	sw $s3, 0($s0)
	add $s0, $gp, 208
	add $s1, $gp, 208
	lw $s2, 0($s1)
	li $s1, 1
	add $s3, $s2, $s1
	sw $s3, 0($s0)
	j .L2
.L3:	 nop
	add $s0, $gp, 204
	li $s1, 0
	sw $s1, 0($s0)
	add $s0, $gp, 212
	li $s1, 0
	sw $s1, 0($s0)
.L4:	nop
	add $s0, $gp, 204
	lw $s1, 0($s0)
	li $s0, 100
	slt $s2, $s1, $s0
	beq $s2, $zero, .L5
	add $s0, $gp, 204
	lw $s1, 0($s0)
	li $s0, 50
	slt $s2, $s1, $s0
	beq $s2, $zero, .L6
	add $s1, $gp, 212
	add $s2, $gp, 212
	lw $s3, 0($s2)
	add $s2, $gp, 204
	lw $s4, 0($s2)
	add $s2, $gp, 4
	sll $s4, $s4, 2
	add $s2, $s2, $s4
	lw $s4, 0($s2)
	add $s2, $s3, $s4
	sw $s2, 0($s1)
	j .L7
.L6:	 nop
	add $s1, $gp, 212
	add $s2, $gp, 212
	lw $s3, 0($s2)
	add $s2, $gp, 204
	lw $s4, 0($s2)
	li $s2, 50
	sub $s5, $s4, $s2
	add $s2, $gp, 216
	sll $s5, $s5, 2
	add $s2, $s2, $s5
	lw $s4, 0($s2)
	add $s2, $s3, $s4
	sw $s2, 0($s1)
.L7:	 nop
	add $s1, $gp, 204
	add $s2, $gp, 204
	lw $s3, 0($s2)
	li $s2, 1
	add $s4, $s3, $s2
	sw $s4, 0($s1)
	j .L4
.L5:	 nop
	add $s1, $gp, 212
	lw $s2, 0($s1)
	move $a0, $s2
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $s1, 5
	add $s2, $gp, 4
	sll $s1, $s1, 2
	add $s2, $s2, $s1
	lw $s1, 0($s2)
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $s1, 10
	add $s2, $gp, 216
	sll $s1, $s1, 2
	add $s2, $s2, $s1
	lw $s1, 0($s2)
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $v0, 10
	syscall
