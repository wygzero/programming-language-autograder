	.data
.newline: .asciiz "\n"
	.text
	.globl main
main:	nop
	move	$fp,$sp
	li $s0, 3
	add $s1, $gp, 4
	sll $s0, $s0, 2
	add $s1, $s1, $s0
	li $s0, 4
	sw $s0, 0($s1)
	li $s0, 3
	add $s1, $gp, 4
	sll $s0, $s0, 2
	add $s1, $s1, $s0
	lw $s0, 0($s1)
	move $a0, $s0
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $v0, 10
	syscall
