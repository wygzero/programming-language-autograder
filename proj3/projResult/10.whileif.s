	.data
.newline: .asciiz "\n"
.string0: .asciiz "Enter a:"
	.text
	.globl main
main:	nop
	move	$fp,$sp
	la $s0, .string0
	move $a0, $s0
	li $v0, 4
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 12
	li $v0, 5
	syscall
	sw $v0, 0($s0)
	add $s0, $gp, 12
	lw $s1, 0($s0)
	li $s0, 2
	div $s2, $s1, $s0
	li $s0, 2
	mul $s1, $s2, $s0
	add $s0, $gp, 12
	lw $s2, 0($s0)
	seq $s0, $s1, $s2
	beq $s0, $zero, .L0
	add $s0, $gp, 8
	li $s2, 1
	sw $s2, 0($s0)
	add $s0, $gp, 4
	li $s2, 0
	sw $s2, 0($s0)
.L1:	nop
	add $s0, $gp, 8
	lw $s2, 0($s0)
	add $s0, $gp, 12
	lw $s3, 0($s0)
	sle $s0, $s2, $s3
	beq $s0, $zero, .L2
	add $s0, $gp, 4
	add $s2, $gp, 4
	lw $s3, 0($s2)
	add $s2, $gp, 8
	lw $s4, 0($s2)
	add $s2, $s3, $s4
	sw $s2, 0($s0)
	add $s0, $gp, 8
	add $s2, $gp, 8
	lw $s3, 0($s2)
	li $s2, 1
	add $s4, $s3, $s2
	sw $s4, 0($s0)
	j .L1
.L2:	 nop
	j .L3
.L0:	 nop
	add $s0, $gp, 8
	li $s2, 1
	sw $s2, 0($s0)
	add $s0, $gp, 4
	li $s2, 1
	sw $s2, 0($s0)
.L4:	nop
	add $s0, $gp, 8
	lw $s2, 0($s0)
	add $s0, $gp, 12
	lw $s3, 0($s0)
	sle $s0, $s2, $s3
	beq $s0, $zero, .L5
	add $s0, $gp, 4
	add $s2, $gp, 4
	lw $s3, 0($s2)
	add $s2, $gp, 8
	lw $s4, 0($s2)
	mul $s2, $s3, $s4
	sw $s2, 0($s0)
	add $s0, $gp, 8
	add $s2, $gp, 8
	lw $s3, 0($s2)
	li $s2, 1
	add $s4, $s3, $s2
	sw $s4, 0($s0)
	j .L4
.L5:	 nop
.L3:	 nop
	add $s0, $gp, 4
	lw $s2, 0($s0)
	move $a0, $s2
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $v0, 10
	syscall
