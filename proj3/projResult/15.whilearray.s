	.data
.newline: .asciiz "\n"
	.text
	.globl main
main:	nop
	move	$fp,$sp
	add $s0, $gp, 4
	li $s1, 0
	sw $s1, 0($s0)
.L0:	nop
	add $s0, $gp, 4
	lw $s1, 0($s0)
	li $s0, 10
	slt $s2, $s1, $s0
	beq $s2, $zero, .L1
	add $s0, $gp, 4
	lw $s1, 0($s0)
	add $s0, $gp, 8
	sll $s1, $s1, 2
	add $s0, $s0, $s1
	add $s1, $gp, 4
	lw $s2, 0($s1)
	sw $s2, 0($s0)
	add $s0, $gp, 4
	add $s1, $gp, 4
	lw $s2, 0($s1)
	li $s1, 1
	add $s3, $s2, $s1
	sw $s3, 0($s0)
	j .L0
.L1:	 nop
	add $s0, $gp, 4
	li $s1, 0
	sw $s1, 0($s0)
.L2:	nop
	add $s0, $gp, 4
	lw $s1, 0($s0)
	li $s0, 10
	slt $s2, $s1, $s0
	beq $s2, $zero, .L3
	add $s0, $gp, 4
	lw $s1, 0($s0)
	li $s0, 5
	slt $s2, $s1, $s0
	beq $s2, $zero, .L4
	add $s1, $gp, 4
	lw $s2, 0($s1)
	add $s1, $gp, 8
	sll $s2, $s2, 2
	add $s1, $s1, $s2
	lw $s2, 0($s1)
	move $a0, $s2
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	j .L5
.L4:	 nop
	li $s1, 0
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
.L5:	 nop
	add $s1, $gp, 4
	add $s2, $gp, 4
	lw $s3, 0($s2)
	li $s2, 1
	add $s4, $s3, $s2
	sw $s4, 0($s1)
	j .L2
.L3:	 nop
	li $v0, 10
	syscall
