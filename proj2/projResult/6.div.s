	.data
.newline: .asciiz "\n"
	.text
	.globl main
main:	nop
	move	$fp,$sp
	li $s0, 51
	li $s1, 17
	div $s2, $s0, $s1
	move $a0, $s2
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 4
	li $s1, 100
	sw $s1, 0($s0)
	add $s0, $gp, 12
	li $s1, 2
	sw $s1, 0($s0)
	add $s0, $gp, 16
	li $s1, 5
	sw $s1, 0($s0)
	add $s0, $gp, 8
	add $s1, $gp, 4
	lw $s2, 0($s1)
	add $s1, $gp, 12
	lw $s3, 0($s1)
	div $s1, $s2, $s3
	add $s2, $gp, 16
	lw $s3, 0($s2)
	div $s2, $s1, $s3
	sw $s2, 0($s0)
	add $s0, $gp, 8
	lw $s1, 0($s0)
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $v0, 10
	syscall
