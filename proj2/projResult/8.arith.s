	.data
.newline: .asciiz "\n"
	.text
	.globl main
main:	nop
	move	$fp,$sp
	add $s0, $gp, 4
	li $s1, 1
	sw $s1, 0($s0)
	add $s0, $gp, 8
	li $s1, 2
	sw $s1, 0($s0)
	add $s0, $gp, 12
	li $s1, 3
	sw $s1, 0($s0)
	add $s0, $gp, 16
	li $s1, 4
	sw $s1, 0($s0)
	add $s0, $gp, 20
	add $s1, $gp, 4
	lw $s2, 0($s1)
	li $s1, 7
	add $s3, $s2, $s1
	sw $s3, 0($s0)
	add $s0, $gp, 20
	lw $s1, 0($s0)
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 24
	add $s1, $gp, 16
	lw $s2, 0($s1)
	li $s1, 5
	sub $s3, $s2, $s1
	sw $s3, 0($s0)
	add $s0, $gp, 24
	lw $s1, 0($s0)
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 8
	lw $s1, 0($s0)
	add $s0, $gp, 12
	lw $s2, 0($s0)
	mul $s0, $s1, $s2
	add $s1, $gp, 16
	lw $s2, 0($s1)
	mul $s1, $s0, $s2
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 8
	lw $s1, 0($s0)
	add $s0, $gp, 12
	lw $s2, 0($s0)
	mul $s0, $s1, $s2
	add $s1, $gp, 16
	lw $s2, 0($s1)
	sub $s1, $s0, $s2
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 4
	lw $s1, 0($s0)
	add $s0, $gp, 8
	lw $s2, 0($s0)
	add $s0, $s1, $s2
	add $s1, $gp, 12
	lw $s2, 0($s1)
	add $s1, $gp, 16
	lw $s3, 0($s1)
	mul $s1, $s2, $s3
	add $s2, $s0, $s1
	move $a0, $s2
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 12
	lw $s1, 0($s0)
	add $s0, $gp, 16
	lw $s2, 0($s0)
	mul $s0, $s1, $s2
	add $s1, $gp, 8
	lw $s2, 0($s1)
	div $s1, $s0, $s2
	add $s0, $gp, 4
	lw $s2, 0($s0)
	sub $s0, $s1, $s2
	move $a0, $s0
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 4
	lw $s1, 0($s0)
	add $s0, $gp, 8
	lw $s2, 0($s0)
	add $s0, $s1, $s2
	add $s1, $gp, 12
	lw $s2, 0($s1)
	add $s1, $gp, 4
	lw $s3, 0($s1)
	add $s1, $gp, 20
	lw $s4, 0($s1)
	mul $s1, $s3, $s4
	add $s3, $s2, $s1
	mul $s1, $s0, $s3
	add $s0, $gp, 24
	lw $s2, 0($s0)
	div $s0, $s1, $s2
	move $a0, $s0
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $v0, 10
	syscall
