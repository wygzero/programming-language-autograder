	.data
.newline: .asciiz "\n"
.string0: .asciiz "i = "
.string1: .asciiz "j = "
.string2: .asciiz "k = "
.string3: .asciiz "l = "
.string4: .asciiz "m = "
	.text
	.globl main
main:	nop
	move	$fp,$sp
	la $s0, .string0
	move $a0, $s0
	li $v0, 4
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 4
	li $v0, 5
	syscall
	sw $v0, 0($s0)
	la $s0, .string1
	move $a0, $s0
	li $v0, 4
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 8
	li $v0, 5
	syscall
	sw $v0, 0($s0)
	la $s0, .string2
	move $a0, $s0
	li $v0, 4
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 12
	li $v0, 5
	syscall
	sw $v0, 0($s0)
	la $s0, .string3
	move $a0, $s0
	li $v0, 4
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 16
	li $v0, 5
	syscall
	sw $v0, 0($s0)
	la $s0, .string4
	move $a0, $s0
	li $v0, 4
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 20
	li $v0, 5
	syscall
	sw $v0, 0($s0)
	add $s0, $gp, 24
	add $s1, $gp, 12
	lw $s2, 0($s1)
	add $s1, $gp, 8
	lw $s3, 0($s1)
	add $s1, $gp, 16
	lw $s4, 0($s1)
	add $s1, $s3, $s4
	slt $s3, $s2, $s1
	add $s1, $gp, 4
	lw $s2, 0($s1)
	add $s1, $gp, 12
	lw $s4, 0($s1)
	sne $s1, $s2, $s4
	xori $s2, $s1, 1
	and $s1, $s3, $s2
	sw $s1, 0($s0)
	add $s0, $gp, 24
	lw $s1, 0($s0)
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 4
	lw $s1, 0($s0)
	add $s0, $gp, 16
	lw $s2, 0($s0)
	mul $s0, $s1, $s2
	add $s1, $gp, 8
	lw $s2, 0($s1)
	add $s1, $gp, 12
	lw $s3, 0($s1)
	mul $s1, $s2, $s3
	sgt $s2, $s0, $s1
	li $s0, 1
	add $s1, $gp, 20
	lw $s3, 0($s1)
	sle $s1, $s0, $s3
	or $s0, $s2, $s1
	move $a0, $s0
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 12
	lw $s1, 0($s0)
	add $s0, $gp, 16
	lw $s2, 0($s0)
	mul $s0, $s1, $s2
	add $s1, $gp, 8
	lw $s2, 0($s1)
	add $s1, $gp, 12
	lw $s3, 0($s1)
	add $s1, $s2, $s3
	add $s2, $gp, 16
	lw $s3, 0($s2)
	add $s2, $s1, $s3
	sgt $s1, $s0, $s2
	add $s0, $gp, 4
	lw $s2, 0($s0)
	add $s0, $gp, 8
	lw $s3, 0($s0)
	sne $s0, $s2, $s3
	and $s2, $s1, $s0
	add $s0, $gp, 16
	lw $s1, 0($s0)
	add $s0, $gp, 24
	lw $s3, 0($s0)
	sgt $s0, $s1, $s3
	or $s1, $s2, $s0
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	add $s0, $gp, 16
	lw $s1, 0($s0)
	add $s0, $gp, 8
	lw $s2, 0($s0)
	div $s0, $s1, $s2
	add $s1, $gp, 4
	lw $s2, 0($s1)
	sub $s1, $s0, $s2
	add $s0, $gp, 20
	lw $s2, 0($s0)
	sgt $s0, $s1, $s2
	add $s1, $gp, 12
	lw $s2, 0($s1)
	add $s1, $gp, 16
	lw $s3, 0($s1)
	add $s1, $gp, 24
	lw $s4, 0($s1)
	mul $s1, $s3, $s4
	add $s3, $s2, $s1
	add $s1, $gp, 16
	lw $s2, 0($s1)
	add $s1, $gp, 12
	lw $s4, 0($s1)
	add $s1, $gp, 24
	lw $s5, 0($s1)
	mul $s1, $s4, $s5
	add $s4, $s2, $s1
	sne $s1, $s3, $s4
	and $s2, $s0, $s1
	move $a0, $s2
	li $v0, 1
	syscall
	li $v0, 4
	la, $a0, .newline
	syscall
	li $v0, 10
	syscall
