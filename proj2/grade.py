import os
import sys
import subprocess

myPath = os.getcwd()
submission_path = os.path.join(myPath, "submission")
resultPath = os.path.join(myPath, "projResult")
marsPath = "/".join(myPath.split("/")[:-1]+["Mars4_5.jar"])

listStudent = os.listdir(submission_path)

listCMFile = ["1.hello", "2.io", "3.add", "4.sub", "5.mul", "6.div", "7.arith", "8.arith", "9.and",
              "10.or", "11.eq", "12.lt", "13.gt", "14.not", "15.log", "16.log", "17.mix", "18.mix", "19.string"]

def unzip():
	for student in listStudent:
		student_name = student.split("_",1)
		if(student_name[0][0] == '.'):
			continue;
		print(student_name[0])
		p = os.path.join(submission_path,student_name[0])
		bashCommand = "cd %s;mkdir %s;tar zxvf %s -C%s"% (submission_path,student_name[0],student,p)
		process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE,shell=True)
		output, error = process.communicate()
	

def make():

    for student in listStudent:
        student_listDir = os.listdir(os.path.join(submission_path, student))
        if any('CminusProject2' in folder for folder in student_listDir):
            # print(os.listdir(os.path.join(submission_path,student))[0])
            p = os.path.join(submission_path, student, os.listdir(
                os.path.join(submission_path, student))[0])
        else:
            p = os.path.join(submission_path, student)
        print(p)
        bashCommand = "cd \"%s\";rm cmc; make" % p
        process = subprocess.Popen(
            bashCommand, stdout=subprocess.PIPE, shell=True)
        output, error = process.communicate()


def getResult():
    path_2in = os.path.join(myPath, '2.in')
    path_18in = os.path.join(myPath, '18.in')
    for assb in listCMFile:
        print(assb)
        if assb == "2.io":
            bashCommand = "cd %s; java -jar %s %s.s < %s > %s.out" % (
                resultPath, marsPath, assb, path_2in, assb)
        elif assb == "18.mix":
            bashCommand = "cd %s; java -jar %s %s.s < %s > %s.out" % (
                resultPath, marsPath, assb, path_18in, assb)
        else:
            bashCommand = "cd %s; java -jar %s %s.s  > %s.out" % (
                resultPath, marsPath, assb, assb)
        print(bashCommand)
        process = subprocess.Popen(
            bashCommand, stdout=subprocess.PIPE, shell=True)
        output, error = process.communicate()
        with open(os.path.join(resultPath, '%s.out' % assb), 'r') as f:
            content = f.readlines()
        with open(os.path.join(resultPath, '%s.out' % assb), 'w') as f:
            f.writelines(content[2:])


def compile():
    for student in listStudent:

        print(student)
        student_listDir = os.listdir(os.path.join(submission_path, student))
        print(student_listDir)
        removeList = [x for x in student_listDir if '.' in x and "Dox" not in x]
        print(removeList)
        for x in removeList:
            bashCommand = "cd \"%s\";rm %s" % (os.path.join(submission_path, student), x)
            print(bashCommand)
            process = subprocess.Popen(
            bashCommand, stdout=subprocess.PIPE, shell=True)
            output, error = process.communicate()
        if any('CminusProject2' in folder for folder in student_listDir):  # normal student
            # print(os.listdir(os.path.join(submission_path,student))[0])
            p = os.path.join(submission_path, student, os.listdir(
                os.path.join(submission_path, student))[0])
        else:  # student dont have CminProject# folder
            p = os.path.join(submission_path, student)
        print(p)
        if os.path.isfile(os.path.join(p, "cmc")):
            print("Processing student: %s" % student)
            for cmfile in listCMFile:
                print("Processing file: %s" % cmfile)
                bashCommand = "cd \"%s\";./cmc ./input/%s.cm" % (p, cmfile)
                process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
                try:
                    output, error = process.communicate()
                except:
                    pass
        else:
            print("False!!!!, %s, %s" % (student, p))


def run_compare():
    path_2in = os.path.join(myPath, '2.in')
    path_18in = os.path.join(myPath, '18.in')
    for student in listStudent:
        counter = 0
        failList = []
        print("Student %s Processing" % student)
        student_listDir = os.listdir(os.path.join(submission_path, student))
        print(student_listDir)
        if any('CminusProject2' in folder for folder in student_listDir):  # normal student
            # print(os.listdir(os.path.join(submission_path,student))[0])
            p = os.path.join(submission_path, student, os.listdir(os.path.join(submission_path, student))[0])
        else:                                                              # student dont have CminProject# folder
            p = os.path.join(submission_path, student)
        print(p)
        if os.path.isfile(os.path.join(p, "cmc")):
            p1 = os.path.join(p,"input")
            print(listCMFile)
            for assb in listCMFile:
                if os.path.isfile(os.path.join(p1, assb+".s")):         # have .s assembly
                    if assb == "2.io":
                        bashCommand = "cd %s; java -jar %s %s.s < %s > %s.out" % (
                        p1, marsPath, assb, path_2in, assb)
                    elif assb == "18.mix":
                        bashCommand = "cd %s; java -jar %s %s.s < %s > %s.out" % (p1, marsPath, assb, path_18in, assb)
                    else:
                        bashCommand = "cd %s; java -jar %s %s.s  > %s.out" % (
                            p1, marsPath, assb, assb)
                    print(bashCommand)
                    try:
                        process = subprocess.Popen(
                            bashCommand, stdout=subprocess.PIPE, shell=True)
                        output, error = process.communicate()
                        with open(os.path.join(p1, '%s.out' % assb), 'r') as f:
                            content = f.readlines()
                        with open(os.path.join(p1, '%s.out' % assb), 'w') as f:
                            f.writelines(content[2:])
                    except:
                        pass
                else:                                               # no .s assembly
                    with open(os.path.join(p1, '%s.out' % assb), 'w') as f:
                        f.writelines("")

            for file in listCMFile:
                try:
                    # print(os.path.join(resultPath,file+".out"))
                    # print(os.path.join(p1,file+".out"))
                    with open(os.path.join(resultPath,file+'.out'), 'r') as f2:
                        result = f2.readlines()
                    try:
                        with open(os.path.join(p1,file+'.out'), 'r') as f2:
                            answer = f2.readlines()
                    except:
                        answer = ""
                    if result == answer:
                        counter+=1
                    else:
                        failList.append(file)
                        pass
                except:
                    print("cannot find file: %s " % file)
                    failList.append(file)
            
            if failList != []:
                print("%s / 19 correct! Wrong files: %s" % (counter, ','.join(failList)))
                rootDir = os.path.join(submission_path, student)
                with open(os.path.join(rootDir,'result'), 'w') as f:
                    f.write("%s / 19 correct! Wrong files: %s\n\n\n" % (counter, ','.join(failList)))
                    for file in failList:
                        with open(os.path.join(resultPath,file+'.out'), 'r') as f2:
                            result = f2.readlines()
                        try:
                            with open(os.path.join(p1,file+'.out'), 'r') as f2:
                                answer = f2.readlines()
                        except:
                            answer = ""
                        f.write("For file: %s \nExpected: \n" % file)
                        f.writelines(result)
                        f.write("---------------------------------------------------\nYour result:\n")
                        f.writelines(answer)
                        f.write("---------------------------------------------------\n")
            else:
                rootDir = os.path.join(submission_path, student)
                with open(os.path.join(rootDir,'result'), 'w') as f:
                    try:
                        f.write("%s / 19 correct! Wrong files: %s" % (counter, ','.join(failList)))
                    except:
                        pass

                                
        else:
            print("False!!!!, %s, %s" % (student, p))
            rootDir = os.path.join(submission_path, student)
            with open(os.path.join(rootDir,'result'), 'w') as f:
                try:
                    f.write("cannot generate cmc!")
                except:
                    pass
                


def run_compare_specific(name):
    path_2in = os.path.join(myPath, '2.in')
    path_18in = os.path.join(myPath, '18.in')
    for student in listStudent:
        if student != name:
            continue;
        counter = 0
        failList = []
        print("Student %s Processing" % student)
        student_listDir = os.listdir(os.path.join(submission_path, student))
        print(student_listDir)
        if any('CminusProject2' in folder for folder in student_listDir):  # normal student
            # print(os.listdir(os.path.join(submission_path,student))[0])
            p = os.path.join(submission_path, student, os.listdir(os.path.join(submission_path, student))[0])
        else:                                                              # student dont have CminProject# folder
            p = os.path.join(submission_path, student)
        print(p)
        if os.path.isfile(os.path.join(p, "cmc")):
            p1 = os.path.join(p,"input")
            print(listCMFile)
            for assb in listCMFile:

                bashCommand = "cd \"%s\";./cmc ./input/%s.cm" % (p, assb)
                process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
                try:
                    output, error = process.communicate()
                except:
                    pass

                if os.path.isfile(os.path.join(p1, assb+".s")):         # have .s assembly
                    if assb == "2.io":
                        bashCommand = "cd %s; java -jar %s %s.s < %s > %s.out" % (
                        p1, marsPath, assb, path_2in, assb)
                    elif assb == "18.mix":
                        bashCommand = "cd %s; java -jar %s %s.s < %s > %s.out" % (p1, marsPath, assb, path_18in, assb)
                    else:
                        bashCommand = "cd %s; java -jar %s %s.s  > %s.out" % (
                            p1, marsPath, assb, assb)
                    print(bashCommand)
                    try:
                        process = subprocess.Popen(
                            bashCommand, stdout=subprocess.PIPE, shell=True)
                        output, error = process.communicate()
                        with open(os.path.join(p1, '%s.out' % assb), 'r') as f:
                            content = f.readlines()
                        with open(os.path.join(p1, '%s.out' % assb), 'w') as f:
                            f.writelines(content[2:])
                    except:
                        pass
                else:                                               # no .s assembly
                    with open(os.path.join(p1, '%s.out' % assb), 'w') as f:
                        f.writelines("")

            for file in listCMFile:
                try:
                    # print(os.path.join(resultPath,file+".out"))
                    # print(os.path.join(p1,file+".out"))
                    with open(os.path.join(resultPath,file+'.out'), 'r') as f2:
                        result = f2.readlines()
                    try:
                        with open(os.path.join(p1,file+'.out'), 'r') as f2:
                            answer = f2.readlines()
                    except:
                        answer = ""
                    if result == answer:
                        counter+=1
                    else:
                        failList.append(file)
                        pass
                except:
                    print("cannot find file: %s " % file)
                    failList.append(file)
            
            if failList != []:
                print("%s / 19 correct! Wrong files: %s" % (counter, ','.join(failList)))
                rootDir = os.path.join(submission_path, student)
                with open(os.path.join(rootDir,'result'), 'w') as f:
                    f.write("%s / 19 correct! Wrong files: %s\n\n\n" % (counter, ','.join(failList)))
                    for file in failList:
                        with open(os.path.join(resultPath,file+'.out'), 'r') as f2:
                            result = f2.readlines()
                        try:
                            with open(os.path.join(p1,file+'.out'), 'r') as f2:
                                answer = f2.readlines()
                        except:
                            answer = ""
                        f.write("For file: %s \nExpected: \n" % file)
                        f.writelines(result)
                        f.write("---------------------------------------------------\nYour result:\n")
                        f.writelines(answer)
                        f.write("---------------------------------------------------\n")
            else:
                rootDir = os.path.join(submission_path, student)
                with open(os.path.join(rootDir,'result'), 'w') as f:
                    try:
                        f.write("%s / 19 correct! Wrong files: %s" % (counter, ','.join(failList)))
                    except:
                        pass

                                
        else:
            print("False!!!!, %s, %s" % (student, p))
            rootDir = os.path.join(submission_path, student)
            with open(os.path.join(rootDir,'result'), 'w') as f:
                try:
                    f.write("cannot generate cmc!")
                except:
                    pass
                



def delete(): 
    for student in listStudent:
        student_listDir = os.listdir(os.path.join(submission_path, student))
        if any('CminusProject2' in folder for folder in student_listDir):
            # print(os.listdir(os.path.join(submission_path,student))[0])
            p = os.path.join(submission_path, student, os.listdir(
                os.path.join(submission_path, student))[0])
        else:  # student dont have CminProject# folder
            p = os.path.join(submission_path, student)
        print(p)
        bashCommand = "cd \"%s\";rm result" % os.path.join(submission_path, student)
        print(bashCommand)
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()
        bashCommand = "cd \"%s\";rm *.s, ;rm *.out;cd ..;rm cmc" % os.path.join(p,"input")
        print(bashCommand)
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()

def delete_result_out(): 
    for student in listStudent:
        student_listDir = os.listdir(os.path.join(submission_path, student))
        if any('CminusProject2' in folder for folder in student_listDir):
            # print(os.listdir(os.path.join(submission_path,student))[0])
            p = os.path.join(submission_path, student, os.listdir(
                os.path.join(submission_path, student))[0])
        else:  # student dont have CminProject# folder
            p = os.path.join(submission_path, student)
        print(p)
        bashCommand = "cd \"%s\";rm result" % os.path.join(submission_path, student)
        print(bashCommand)
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()
        bashCommand = "cd \"%s\";rm *.out" % os.path.join(p,"input")
        # print(bashCommand)
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()


if __name__ == '__main__':
    # getResult()
    
    # delete()

    # make()
    # compile()
    # delete_result_out()
    # run_compare()

    run_compare_specific("burnskirstin")
