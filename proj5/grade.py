

import os
import sys
import subprocess

myPath = os.getcwd()
submission_path = os.path.join(myPath, "submission")
resultPath = os.path.join(myPath, "projResult")
listTest = ["last","wrap","cpa","ira","invert","filter","summatrices","swapper","flatten","bti","rember","depth"]
listRun = ["last","wrap","cpa","ira","invert","filter","summatrices","swapper","flatten","bti","rember","depth","lazy"]


listStudent = os.listdir(submission_path)

def makeDir():
    student_List = []
    for student in listStudent:
        student_name = student.split("_",1)
        p = os.path.join(submission_path,student_name[0])
        if student_name in student_List:
            bashCommand = "cd %s;mv %s %s;"% (submission_path,student, student_name[0])
            # print(bashCommand)
        else:
            bashCommand = "cd %s;mkdir %s;mv %s %s;"% (submission_path,student_name[0],student, student_name[0])
            # print(bashCommand)
        
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()
        student_List.append(student_name)

def getResult():
    for test in listTest:
        inFileName = test+"_test.rkt"
        outFileName = test+".rkt"
        resultFileName = "result_"+test+".txt"
        bashCommand = "cd %s;rm %s;cat %s %s > %s;"% (resultPath,os.path.join(resultPath,outFileName),os.path.join(resultPath,"program5.rkt"),os.path.join(myPath,inFileName), os.path.join(resultPath,outFileName))
        print(bashCommand)
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()

        bashCommand = "cd %s;rm %s;racket %s > %s;"% (resultPath,os.path.join(resultPath,resultFileName),os.path.join(resultPath,outFileName), os.path.join(resultPath,resultFileName))
        print(bashCommand)
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()

def cat():
    for student in listStudent:
        p = os.path.join(submission_path,student)
        p_test1 = os.path.join(myPath,"p5test.rkt")
        p_test2 = os.path.join(myPath,"lazy_test.rkt")
        bashCommand = "rm %s; rm %s;rm %s;"%(os.path.join(p,"lazy.rkt"), os.path.join(p,"result_*.txt"), os.path.join(p,"0_result.txt"))
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()
        for test in listTest:
            outFileName = test+".rkt"
            bashCommand = "rm %s"%(os.path.join(p,outFileName))
            process = subprocess.Popen(bashCommand, stdout=None, shell=True)
            output, error = process.communicate()
        listFile = os.listdir(p)
        print(listFile)
        for file in listFile:
            if 'program' in file or 'Program' in file or 'proj' in file or 'Proj' in file:
                print("prog5 exist")
                for test in listTest:
                    inFileName = test+"_test.rkt"
                    outFileName = test+".rkt"
                    print(outFileName)
                    bashCommand = "cd %s;cat %s %s > %s;"% (p,os.path.join(p,file),os.path.join(myPath,inFileName), os.path.join(p,outFileName))
                    process = subprocess.Popen(bashCommand, stdout=None, shell=True)
                    output, error = process.communicate()
            elif 'lazy' in file:
                print("lazy exist")
                # print(file)
                bashCommand = "cd %s;cat %s %s > %s;"% (p,os.path.join(p,file),p_test2, os.path.join(p,"lazy.rkt"))
                print(bashCommand)
                process = subprocess.Popen(bashCommand, stdout=None, shell=True)
                output, error = process.communicate()
            else:
                print("attention!!! %s, %s!!"%(student,file))
                
def cat_specific(name):

    for student in listStudent:
        if student != name:
            continue
        p = os.path.join(submission_path,student)
        p_test1 = os.path.join(myPath,"p5test.rkt")
        p_test2 = os.path.join(myPath,"lazy_test.rkt")
        bashCommand = "rm %s; rm %s;rm %s;"%(os.path.join(p,"lazy.rkt"), os.path.join(p,"result_*.txt"), os.path.join(p,"0_result.txt"))
        process = subprocess.Popen(bashCommand, stdout=None, shell=True)
        output, error = process.communicate()
        for test in listTest:
            outFileName = test+".rkt"
            bashCommand = "rm %s"%(os.path.join(p,outFileName))
            process = subprocess.Popen(bashCommand, stdout=None, shell=True)
            output, error = process.communicate()
        listFile = os.listdir(p)
        print(listFile)
        for file in listFile:
            if 'program' in file or 'Program' in file or 'proj' in file or 'Proj' in file:
                print("prog5 exist")
                for test in listTest:
                    inFileName = test+"_test.rkt"
                    outFileName = test+".rkt"
                    print(outFileName)
                    bashCommand = "cd %s;cat %s %s > %s;"% (p,os.path.join(p,file),os.path.join(myPath,inFileName), os.path.join(p,outFileName))
                    process = subprocess.Popen(bashCommand, stdout=None, shell=True)
                    output, error = process.communicate()
            elif 'lazy' in file:
                print("lazy exist")
                # print(file)
                bashCommand = "cd %s;cat %s %s > %s;"% (p,os.path.join(p,file),p_test2, os.path.join(p,"lazy.rkt"))
                print(bashCommand)
                process = subprocess.Popen(bashCommand, stdout=None, shell=True)
                output, error = process.communicate()
            else:
                print("attention!!! %s, %s!!"%(student,file))


def run_output():
    for student in listStudent:
        counter = 0
        failList = []
        p = os.path.join(submission_path,student)
        listFile = os.listdir(p)
        # print(p)
        # print(listFile)
        print("Student %s Processing" % student)
        for run in listRun:
            runFileName = run+".rkt"
            resultFileName = "result_"+run+".txt"
            if runFileName in listFile:
                bashCommand = "cd %s; rm %s;racket %s > %s"%(p,resultFileName,runFileName,resultFileName)
                print(bashCommand)
                try:
                    process = subprocess.Popen(
                    bashCommand, stdout=subprocess.PIPE, shell=True)
                    output, error = process.communicate()
                except:
                    pass
            else:
                with open(os.path.join(p,resultFileName),'w') as f:
                    f.writelines("")

        for run in listRun:
            resultFileName = "result_"+run+".txt"
            try:
                with open(os.path.join(resultPath,resultFileName), 'r') as f2:
                    result = f2.readlines()
                try:
                    with open(os.path.join(p,resultFileName), 'r') as f2:
                        answer = f2.readlines()
                except:
                    answer = ""
                if result == answer:
                    counter+=1
                else:
                    failList.append(run)
                    pass
            except:
                print("cannot find file: %s " % run)
                failList.append(run)

        if failList != []:
            print("%s / 13 correct! Wrong tests: %s" % (counter, ','.join(failList)))
            rootDir = p
            with open(os.path.join(rootDir,'0_result.txt'), 'w') as f:
                f.write("%s / 13 correct! Wrong test: %s\n\n\n" % (counter, ','.join(failList)))
                for file in failList:
                    testFileName = file+"_test.rkt"
                    resultFileName = "result_"+file+".txt"
                    with open(os.path.join(resultPath,resultFileName), 'r') as f2:
                        result = f2.readlines()
                    with open(os.path.join(myPath,testFileName), 'r') as f2:
                        testCase = f2.readlines()                    
                    try:
                        with open(os.path.join(p,resultFileName), 'r') as f2:
                            answer = f2.readlines()
                    except:
                        answer = ""
                    f.write("For test: %s \nExpected: \n" % file)
                    f.writelines(result)
                    f.write("---------------------------------------------------\nYour result:\n")
                    f.writelines(answer)
                    f.write("---------------------------------------------------\nTest Cases:\n")
                    f.writelines(testCase)
                    f.write("---------------------------------------------------")

        else:
            rootDir = os.path.join(submission_path, student)
            with open(os.path.join(rootDir,'0_result.txt'), 'w') as f:
                try:
                    f.write("%s / 13 correct! Wrong tests: %s" % (counter, ','.join(failList)))
                except:
                    pass


def run_output_specific(name):
    for student in listStudent:
        if student != name:
            continue
        counter = 0
        failList = []
        p = os.path.join(submission_path,student)
        listFile = os.listdir(p)
        # print(p)
        # print(listFile)
        print("Student %s Processing" % student)
        for run in listRun:
            runFileName = run+".rkt"
            resultFileName = "result_"+run+".txt"
            if runFileName in listFile:
                bashCommand = "cd %s; rm %s;racket %s > %s"%(p,resultFileName,runFileName,resultFileName)
                print(bashCommand)
                try:
                    process = subprocess.Popen(
                    bashCommand, stdout=subprocess.PIPE, shell=True)
                    output, error = process.communicate()
                except:
                    pass
            else:
                with open(os.path.join(p,resultFileName),'w') as f:
                    f.writelines("")

        for run in listRun:
            resultFileName = "result_"+run+".txt"
            try:
                with open(os.path.join(resultPath,resultFileName), 'r') as f2:
                    result = f2.readlines()
                try:
                    with open(os.path.join(p,resultFileName), 'r') as f2:
                        answer = f2.readlines()
                except:
                    answer = ""
                if result == answer:
                    counter+=1
                else:
                    failList.append(run)
                    pass
            except:
                print("cannot find file: %s " % run)
                failList.append(run)

        if failList != []:
            print("%s / 13 correct! Wrong tests: %s" % (counter, ','.join(failList)))
            rootDir = p
            with open(os.path.join(rootDir,'0_result.txt'), 'w') as f:
                f.write("%s / 13 correct! Wrong test: %s\n\n\n" % (counter, ','.join(failList)))
                for file in failList:
                    testFileName = file+"_test.rkt"
                    resultFileName = "result_"+file+".txt"
                    with open(os.path.join(resultPath,resultFileName), 'r') as f2:
                        result = f2.readlines()
                    with open(os.path.join(myPath,testFileName), 'r') as f2:
                        testCase = f2.readlines()                    
                    try:
                        with open(os.path.join(p,resultFileName), 'r') as f2:
                            answer = f2.readlines()
                    except:
                        answer = ""
                    f.write("For test: %s \nExpected: \n" % file)
                    f.writelines(result)
                    f.write("---------------------------------------------------\nYour result:\n")
                    f.writelines(answer)
                    f.write("---------------------------------------------------\nTest Cases:\n")
                    f.writelines(testCase)
                    f.write("---------------------------------------------------")

        else:
            rootDir = os.path.join(submission_path, student)
            with open(os.path.join(rootDir,'0_result.txt'), 'w') as f:
                try:
                    f.write("%s / 13 correct! Wrong tests: %s" % (counter, ','.join(failList)))
                except:
                    pass

def specific(name):
    cat_specific(name)
    run_output_specific(name)




if __name__ == '__main__':
    # makeDir()
    # getResult()
    # cat()
    # run_output()
    specific("winklemanemily")