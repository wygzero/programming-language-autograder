
(let ([cont (lambda (v) (printf "error in call/cc\n"))])
  (with-handlers ((exn? (lambda (exn) (display (exn-message exn)) (newline) (cont #f))))
    (begin
   
 
      (call/cc (lambda (k) (set! cont k)       (display (summatrices '() '())) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (summatrices '((1 2 3)) '((4 5 6)))) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (summatrices '((1 2 3) (4 5 6)) '((10 10 10) (20 20 20)))) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (summatrices '((1) (2) (3) (4) (5)) '((10) (20) (30) (40) (50)))) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (summatrices '((1 2) (3 4) (5 6)) '((-1 -2) (-3 -4) (-5 -6)))) (newline) ))
      
    
    )))
