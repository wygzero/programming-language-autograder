
(let ([cont (lambda (v) (printf "error in call/cc\n"))])
  (with-handlers ((exn? (lambda (exn) (display (exn-message exn)) (newline) (cont #f))))
    (begin
     
   
      (call/cc (lambda (k) (set! cont k)       (display (invert '())) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (invert '((1 a)))) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (invert '((1 a) (2 b)))) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (invert '((1 a) (2 b) (3 c) (4 d)))) (newline) ))
      (call/cc (lambda (k) (set! cont k)       (display (invert '((1 ()) (2 ()) (() a) (() b)))) (newline) ))
 
    


    )))
