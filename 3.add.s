	.data
.newline: .asciiz "\n"
	.text
	.globl main
main:	nop
        move $fp, $sp
	sub, $sp, $sp, 16
	li $s0, 10
	li $s1, 20
	add $s2, $s0, $s1
	move $a0, $s2
	li $v0, 1
	syscall
	li $v0, 4
	la $a0, .newline
	syscall
	li $s0, 1
	sw $s0, 0($fp)
	li $s0, 3
	sub $s1, $fp, 8
	sw $s0, 0($s1)
	li $s0, 4
	sub $s1, $fp, 12
	sw $s0, 0($s1)
	sub $s0, $fp, 0
	lw $s1, 0($s0)
	sub $s0, $fp, 8
	lw $s2, 0($s0)
	add $s0, $s1, $s2
	sub $s1, $fp, 12
	lw $s2, 0($s1)
	add $s1, $s0, $s2
	sub $s0, $fp, 4
	sw $s1, 0($s0)
	sub $s0, $fp, 4
	lw $s1, 0($s0)
	move $a0, $s1
	li $v0, 1
	syscall
	li $v0, 4
	la $a0, .newline
	syscall
