import os, sys
import subprocess

path = "/home/wygzero/Desktop/CS4120ta/proj2/submissions/"
resultPath = "/home/wygzero/Desktop/CS4120ta/proj2/projResult"

listStudent = os.listdir(path)

listCMFile = ["1.hello", "2.io", "3.add", "4.sub", "5.mul", "6.div", "7.arith", "8.arith", "9.and", "10.or", "11.eq", "12.lt", "13.gt", "14.not", "15.log", "16.log", "17.mix", "18.mix", "19.string"]

def make():
	for student in listStudent:
		if os.listdir(path+student)[0][0] == '.':
			p = os.path.join(path,student,os.listdir(path+student)[1])
		else:
			p = os.path.join(path,student,os.listdir(path+student)[0])

		bashCommand = "cd \"%s\";make" % p

		process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE,shell=True)
		output, error = process.communicate()

def compile():
	for student in listStudent:
		if os.listdir(path+student)[0][0] == '.':
			p = os.path.join(path,student,os.listdir(path+student)[1])
		else:
			p = os.path.join(path,student,os.listdir(path+student)[0])

		if os.path.isfile(os.path.join(p,"cmc")):
			print("Processing student: %s" % student)
			for cmfile in listCMFile:
				print("Processing file: %s" % cmfile)
				bashCommand = "cd \"%s\";./cmc ./input/%s.cm" % (p,cmfile)
				process = subprocess.Popen(bashCommand,stdout=subprocess.PIPE,stdin=subprocess.PIPE,shell=True)
				try:
					if cmfile == "2.io":
						output, error = process.communicate(input="1\n")
					elif cmfile == "18.mix":
						output, error = process.communicate(input="1\n2\n3\n4\n5\n")
					else:
						output, error = process.communicate()
				except:
					pass
		else:
			print("False, %s, %s" % (student, p))

def getResult():
	path_2in = '/home/wygzero/Desktop/CS4120ta/proj2/2.in'
	path_18in = '/home/wygzero/Desktop/CS4120ta/proj2/18.in'
	for assb in listCMFile:
		print(assb)
		
		if assb == "2.io":
			bashCommand = "cd %s; spim -file %s.s < %s > %s.out" % (resultPath,assb,path_2in,assb)
		elif assb == "18.mix":
			bashCommand = "cd %s; spim -file %s.s < %s > %s.out" % (resultPath,assb,path_18in,assb)
		else:
			bashCommand = "cd %s; spim -file %s.s > %s.out" % (resultPath,assb,assb)
		print(bashCommand)
		process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE,shell=True)
		output, error = process.communicate()
		with open(os.path.join(resultPath,'%s.out'%assb), 'r') as f:
			content = f.readlines()
		with open(os.path.join(resultPath,'%s.out'%assb), 'w') as f:
			f.writelines(content[5:])



	


def compile_2():
	cmfile="2.io"
	for student in listStudent:
		if os.listdir(path+student)[0][0] == '.':
			p = os.path.join(path,student,os.listdir(path+student)[1])
		else:
			p = os.path.join(path,student,os.listdir(path+student)[0])

		if os.path.isfile(os.path.join(p,"cmc")):
			print("Processing student: %s" % student)
			bashCommand = "cd \"%s\";./cmc ./input/%s.cm" % (p,cmfile)
			process = subprocess.Popen(bashCommand,stdout=subprocess.PIPE,shell=True)
			try:
				output, error = process.communicate(timeout=5)
			except:
				pass
		else:
			print("False, %s, %s" % (student, p))

def delete():
	for student in listStudent:
		if os.listdir(path+student)[0][0] == '.':
			p = os.path.join(path,student,os.listdir(path+student)[1],"input")
		else:
			p = os.path.join(path,student,os.listdir(path+student)[0],"input")

		bashCommand = "cd \"%s\";rm *.s" % p
		process = subprocess.Popen(bashCommand, stdout=None,shell=True)
		output, error = process.communicate()



if __name__ == '__main__':
	# make()
	# delete()
	# compile()
	#compile_2()
	getResult()